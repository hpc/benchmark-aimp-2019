#!/bin/sh
#SBATCH --job-name=test-stream
#SBATCH --cpus-per-task=20
#SBATCH --tasks=1
#SBATCH --partition=shared
#SBATCH --time=12:00:00
##SBATCH --nodelist=node[209-212]

module load ifort/2016.3.210-GCC-5.4.0-2.26  impi/5.1.3.181 STREAM/5.10

echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo MEM=$MEM

srun -u stream_1Kx1B
