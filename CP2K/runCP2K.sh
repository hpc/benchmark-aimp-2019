#!/bin/sh
#SBATCH --job-name=test-cp2k
#SBATCH --cpus-per-task=1
#SBATCH --tasks=576
#SBATCH --partition=mono-shared
#SBATCH --time=1:00:00
##SBATCH --constraint=E5-2630V4

module load ifort/2018.1.163-GCC-6.4.0-2.28 impi/2018.1.163 CP2K/6.1


# *** WARNING in dbcsr/mm/dbcsr_mm.F:268 :: Using a non-square number of ***
# *** MPI ranks might lead to poor performance.                          ***
# *** Used ranks: 300                                                    ***
# *** Suggested: 289 576                                                 ***


BASE_INP=$EBROOTCP2K/tests/QS/benchmark

INP=$BASE_INP/H2O-512.inp

echo INP=$INP
echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo JOBID=$SLURM_JOB_ID
echo MEM=$MEM

srun cp2k.popt -i $INP -o cp2k-$SLURM_JOB_ID.out
