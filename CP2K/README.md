Description
===========
CP2K is a freely available (GPL) program, written in Fortran 95, to perform atomistic and molecular
 simulations of solid state, liquid, molecular and biological systems. It provides a general framework for different
 methods such as e.g. density functional theory (DFT) using a mixed Gaussian and plane waves approach (GPW), and
 classical pair and many-body potentials.

Homepage
--------
[www.cp2k.org](http://www.cp2k.org)

Compilation
-----------
You need to use GCC compiler version 6 or higher with openMPI support.

You may need to add the flag `-ltr` to the variable `$LIB` in the `Makefile`.

Version
-------
6.1

Running
-------
You must run it as a SLURM batch job.
Accordingly to the user manual of CP2K, we should choose a square number of MPI ranks or we'll have poor performances.


1. Run with 576 MPI workers, 1 thread per core. \ 
   H2O-64

1. Run with 576 MPI workers, 1 thread per core. \ 
   H2O-512

1. Run with 576 MPI workers, 1 thread per core. \
   H2O-1024

1. Run with 576 MPI workers, 1 thread per core. \ 
   H2O-2048


    sbatch runCP2K.sh
   
Results
-------
We want the full log of each run
