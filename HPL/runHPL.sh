#!/bin/sh
#SBATCH --job-name=test-hpl
#SBATCH --cpus-per-task=1
#SBATCH --tasks=800
#SBATCH --partition=parallel
#SBATCH --time=12:00:00

module load GCC/7.3.0-2.30  OpenMPI/3.1.1 HPL/2.2

echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES

srun xhpl 
