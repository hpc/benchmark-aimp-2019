Description
===========
HPL is a software package that solves a (random) dense linear system in double precision (64 bits)
arithmetic on distributed-memory computers. It can thus be regarded as a portable as well as freely available
implementation of the High Performance Computing Linpack Benchmark.

Homepage
--------
[www.netlib.org/benchmark/hpl](http://www.netlib.org/benchmark/hpl)

Version
-------
2.3

Compilation
-----------
You need to use GCC compiler version 6 or bigger or Intel with MPI support.

Running
----------
You must run it as a SLURM batch job.

1. Run it on the whole cluster (compute node only). \ 
   You are responsible to tune the file `HPl.dat`

```
sbatch runHPL.sh
```     

Results
-------
Please provide the full output of the run.

Plesae provide the `HPL.dat` file used.

If you aren't running the benchmark on the whole cluster but doing an extrapolation/simulation, please specify it on the results
