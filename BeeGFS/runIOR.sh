#!/bin/sh
#SBATCH --partition=mono-shared
#SBATCH --time=30:00
#SBATCH --output=slurm-ior-twenty-task-one-node-bs-6G-%j.out
#SBATCH --ntasks=40
##SBATCH --nodes=1
##SBATCH -c 4

module load GCC/7.3.0-2.30  OpenMPI/3.1.1 ior/3.2.0-mpiio

# 3 * RAM_SIZE_PER_STORAGE_SERVER * NUM_STORAGE_SERVERS) / ${NUM_PROCS})
# For example with ntasks = 1:
# 3 * 96G * 2 / 1 = 576G

BLOCK_SIZE=6G
ITERATIONS=1
OUTPUT_FILE=/home/sagon/${SLURM_JOB_ID}.ior


# write on a separate file per process
srun ior -r -w -i${ITERATIONS} -t2m -b ${BLOCK_SIZE} -g -F -e -o $OUTPUT_FILE


# write on the same file per process
srun ior -r -w -i${ITERATIONS} -t2m -b ${BLOCK_SIZE} -g  -e -o $OUTPUT_FILE
