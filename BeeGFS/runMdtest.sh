#!/bin/sh
#SBATCH --partition=mono-shared
#SBATCH --time=15:00
#SBATCH --ntasks=20
##SBATCH --nodes=2
#SBATCH --output=slurm-mdtest-twenty-task-one-node-files-per-dir-782-%j.out
##SBATCH -c 16

module load GCC/7.3.0-2.30  OpenMPI/3.1.1 ior/d49d34-mpiio

# ${NUM_DIRS} = (parameter -b ^ parameter -z)
# -b 8
# -z 2
# NUM_DIRS = 64
#
# Compute FILES_PER_DIR
# The total amount of files should always be higher than 1 000 000, so ${FILES_PER_DIR} is calculated as ${FILES_PER_DIR} = (1000000 / ${NUM_DIRS} / ${NUM_PROCS}).
# Example: FILES_PER_DIR = (1000000 / 64 / 8) = 1954


DIRNAME=/home/sagon/mdtest

#FILES_PER_DIR=15625
#FILES_PER_DIR=782
#FILES_PER_DIR=390

ITERATIONS=5

# be sure that the DIRNAME is empty. Mdtest should take care of removing the data when he completes successfuly.
#if [ -d $DIRNAME ]; then
#  rm -rf $DIRNAME
#fi

srun mdtest -C -T -r -F -d ${DIRNAME} -i ${ITERATIONS}=1 -I ${FILES_PER_DIR} -z 2 -b 8 -L -u

