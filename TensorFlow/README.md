Description
=======
TensorFlow is an end-to-end open source platform for machine learning.

Homepage
--------
[www.tensorflow.org](https://www.tensorflow.org)

Version
-------
1.13

Compilation
-----------
You need to use GCC compiler version 6 or bigger with openMPI and CUDA support
 
The benchmarks used here are available [here](https://github.com/tensorflow/benchmarks) 

```
git clone https://github.com/tensorflow/benchmarks
cd benchmarks
git checkout cnn_tf_v1.13_compatible
``` 


Running
-------
1. tf_cnn_benchmarks.py on 1 GPU simple precision

2. tf_cnn_benchmarks.py on 4 GPU simple precision

3. tf_cnn_benchmarks.py on 1 GPU double precision

4. tf_cnn_benchmarks.py on 4 GPU double precision

Every benchmarks with a batch size of 64.

    sbatch runTFBench.sh
   
Results
---------
The RAW value is the total images/sec. \
Please provide the full output log
