#!/bin/sh
#SBATCH --time=10:00
#SBATCH --gres=gpu:pascal:1
#SBATCH --partition=shared-gpu
#SBATCH --cpus-per-task=1
#SBATCH --mem=12G


module load GCC/7.3.0-2.30  CUDA/9.2.88  OpenMPI/3.1.1 TensorFlow/1.10.1-Python-2.7.15 

INPUT_PATH=benchmarks/scripts/tf_cnn_benchmarks

srun python $INPUT_PATH/tf_cnn_benchmarks.py --num_gpus=1 --batch_size=96 --model=resnet50 --variable_update=parameter_server
