# benchmark-aimp-2019

Annex to the UNIGE HPC cluster AIMP 2019 related to the PL12146.
You will find the details of every benchmarks to be executed below.

To compute a composite global score, we will:

 1. Normalize each by the results we obtained on a reference machine
 2. Multiply the result by 100
 3. Compute the weighted average mean

The weighting of each benchmark in the composite average score is indicated
between square brackets:

Synthetic benchmarks:

 * [HPL](HPL/README.md) [14%]
 * [STREAM](STREAM/README.md) [14%]
 * [BeeGFS-IOR](BeeGFS/README.md) [30%]

User application benchmarks:

 * [Palabos](Palabos/README.md) [14%]
 * [CP2K](CP2K/README.md) [14%]
 * [TensorFlow](TensorFlow/README.md) [14%]
 
## Nota Bene
 
You can adapt the provided SLURM scripts to take into account the
constraints, versions, executable names, etc that you tested. Please,
attach the modified version with the benchmark results.


