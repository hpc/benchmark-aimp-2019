#!/bin/sh
#SBATCH --job-name=test-palabos
#SBATCH --cpus-per-task=1
#SBATCH --tasks=16
#SBATCH --time=6:00

module load foss/2018b

# resolution http://wiki.palabos.org/plb_wiki:benchmarks
N=2500


echo N=$N
echo NTASKS=$SLURM_NTASKS
echo SLURM_NODELIST=$SLURM_NODELIST
echo SLURM_NNODES=$SLURM_NNODES
echo MEM=$MEM

srun palabos-v2.0r0/examples/benchmarks/cavity3d/cavity3d $N
